name, work, movie = "Jean", "Fullstack Developer", "The Last Gift"
age, rating = 28, 88.5

print(f"I am {name}, and I am {age} years old, I work as a {work}, and my rating for {movie} is {rating}%.")

num1, num2, num3 = 2, 4, 6

print("The product of num1 and num2 is: " + str(num1 * num2))
print("is num1 is less than num 3?: " + str(num1 < num3))
print("The sum of num3 and num2 is: " + str(num3 + num2))
